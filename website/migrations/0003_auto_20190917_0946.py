# Generated by Django 2.2.2 on 2019-09-17 07:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0002_auto_20190917_0940'),
    ]

    operations = [
        migrations.AddField(
            model_name='websettings',
            name='primary_color',
            field=models.CharField(default='000000', help_text='Enter company primary color hexa code without #', max_length=6),
        ),
        migrations.AddField(
            model_name='websettings',
            name='secondary_color',
            field=models.CharField(default='000000', help_text='Enter company secondary color hexa code without #', max_length=6),
        ),
    ]
