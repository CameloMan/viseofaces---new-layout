/* Float Label Pattern Plugin for Bootstrap 3.1.0 by Travis Wilson
**************************************************/
var valid = false;
var tries = 0;
var MAX_TRIES = 3;
var firstTime = true;
var timeLeft = 15;
var elem = document.getElementById('coDown');
var timerId = setInterval(countdown, 1000);

function fileValidation(){
    var fileInput = document.getElementById('cardInput');
    if(fileInput.value==""){
        //alert('Por favor ingrese un archivo en formato JPG/PNG');
        fileInput.value = '';
        return false;
    }else{
       return true;
	}
}

function previewFile() {
  var preview = document.querySelector('#cardPreview');
  var file    = document.querySelector('input[type=file]').files[0];
  var reader  = new FileReader();

  reader.onloadend = function () {
    preview.src = reader.result;
  }

  if (file) {
    reader.readAsDataURL(file);
  } else {
    preview.src = "";
  }
}

//-------------------------------------------------------------


function enableCam() {
	// Grab elements, create settings, etc.
	var video = document.getElementById('video');

	// Get access to the camera!
	if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
	    // Not adding `{ audio: true }` since we only want video now
	    navigator.mediaDevices.getUserMedia({ video: true }).then(function(stream) {
	        //video.src = window.URL.createObjectURL(stream);
	        video.srcObject = stream;
	        video.play();
	    });
	}
}



function nextStep(steps){
	// var flag=checkSteps(steps);
	// console.log(flag);
	if(steps==5){
		if(fileValidation()){
				$("#step-"+steps).fadeOut("slow", function() {
					$("#progressBar").css("width", "66%");
					previewFile();	
				
					// create video element
					enableCam();
					$("#step-"+(steps+1)).fadeIn("fast");
					var video = document.getElementById('video');
					video.play();
				});
			}
	}else{
		if(checkSteps(steps)){
			console.log("aqui");
			$("#step-"+steps).fadeOut("slow", function() {
				$("#progressBar").css("width", "33%");
				$("#step-"+(steps+1)).fadeIn("fast");
			});
			$("#step-"+(steps+1)+" :input, select" ).focusout(function() {
				checkSteps(steps+1);
			});
		}
	}
}
function validate_photo(cardPhoto, camFrame) {//THIS //DONE
	$.ajax({
		url: 'ajax/validate_photo/',
		type: 'post',
		data: {
			'cardPhoto': cardPhoto,
			'camFrame': camFrame,
			'firstName': $("#first_name").val(),
			'lastName': $("#last_name").val(),  
		},
		dataType: 'json',
		success: function (data) {
			console.log(data.match);
			if(data.match){
				valid = true;
			} else if(tries < MAX_TRIES) {
				tries++;
				var camFrame = captureFrame(document.getElementById('video'));
				validate_photo(cardPhoto, camFrame);
			}

			if(tries >= MAX_TRIES) {
				alert("No se han encontrado coincidencias volviendo al inicio.");
				window.location.href = "/";
				//showStatusPopup(valid, data.modal_title, data.modal_message, 3000, true);
			}else if(valid){
				alert("Identificacion validada puedes continuar.");
			}
		}
	});
}

function captureFrame(video) { //THIS //DONE
    var canvas = document.createElement("canvas");
    canvas.width = video.videoWidth;
    canvas.height = video.videoHeight;
    var canvasContext = canvas.getContext("2d");
    canvasContext.drawImage(video, 0, 0);
    return canvas.toDataURL('image/png');
}

async function onPlay(videoEl) { //THIS //DONE

	var cardPhoto = document.querySelector('#cardPreview').src;
	var camFrame = captureFrame(document.getElementById('video'));

	if(firstTime && camFrame.length > 20000) {
		console.log(camFrame.length);
		validate_photo(cardPhoto, camFrame);
		firstTime = false;
	}
	

	setTimeout(() => onPlay(videoEl));
}

function showStatusPopup(success, title, message, millis, redirect) { //THIS //DONE
	if(success) {
		$("#successIcon").css({ 'display' : ''});
		$("#failureIcon").css({ 'display' : 'none' });
	} else {
		$("#successIcon").css({ 'display' : 'none' });
		$("#failureIcon").css({ 'display' : '' });
	}

	$("#modalTitle").text(title);
	$("#modalMessage").text(message);

	$("#modal").fadeIn("slow", function() {

		setTimeout(function(){
			$("#modal").fadeOut("slow", function() {
				if(redirect) {
					 window.location.href = "/";
				}
			});
		}, millis); 
		
	});
}
function countdown() {
    if (timeLeft == -1) {
        clearTimeout(timerId);
        doSomething();
    } else {
        elem.innerHTML = timeLeft;
        timeLeft--;
    }
}

function doSomething() {
    //alert("Hi");
}





//-------------------------------------------Validaciones----------------------------------------

function checkSteps(step){
    var salida=true;
    var validaciones= new Array();
    switch(step) {      
        case 0:
            salida = $("#first_name").val().trim()=='' ? false : true;
            changeField(validaciones, "first_name", salida);
            salida = $("#last_name").val().trim()=='' ? false : true;
            changeField(validaciones, "last_name", salida);
            salida = /^[^@]+@[^@]+\.[a-zA-Z]{2,}$/.test($("#email").val());
            changeField(validaciones, "email", salida);
            salida = (/^(\+34|0034|34)?[6|7|8|9][0-9]{8}$/.test($("#phone").val().replace(/ /g,''))||/^(\+809|0034|34)?[0-9]{7}$/.test($("#phone").val().replace(/ /g,'')));
            changeField(validaciones, "phone", salida);
            break;
        case 1:
            salida = $("#sexo").val().trim()=="" ? false : true;
            changeField(validaciones, "sexo", salida);
            salida = $("#estado_civil").val().trim()=="" ? false : true;
            changeField(validaciones, "estado_civil", salida);
            salida = $("#birth_year").val().trim()=="" ? false : true;
            changeField(validaciones, "birth_year", salida);
            salida = $("#birth_month").val().trim()=="" ? false : true;
            changeField(validaciones, "birth_month", salida);
            salida = $("#birth_date").val().trim()=="" ? false : true;
            changeField(validaciones, "birth_date", salida);
            if(!validaciones.includes(false)){
                salida = calcularEdad($("#birth_year").val(),$("#birth_month").val(),$("#birth_date").val());
                changeField(validaciones, "fecha", salida);                
            }
            salida = $("#ciudadania").val().trim()=="" ? false : true;
            changeField(validaciones, "ciudadania", salida);
            salida = $("#direccion").val().trim()=='' ? false : true;
            changeField(validaciones, "direccion", salida);
            break;
        case 2:
            salida = $("#documentType").val().trim()=="" ? false : true;
            changeField(validaciones, "documentType", salida);         
            if($("#documentType").val()=='Cédula'){
                salida = validate($("#documentNumber").val())||/^\d{3}-?\d{7}-?\d{1}$/.test($("#documentNumber").val());
            }else{
                salida = /^[a-z]{3}[0-9]{6}[a-z]?$/i.test($("#documentNumber").val())||  /^\d{9}$/.test($("#documentNumber").val());  
            }                           
            changeField(validaciones, "documentNumber", salida);
            salida = $("#residencia").val().trim()=="" ? false : true;
            changeField(validaciones, "residencia", salida);
            salida =fileValidation();
            changeField(validaciones, "cardInput", salida);
            if(!validaciones.includes(false)){
	            enableCam();
	            previewFile();            	
            }
            break;
        case 3:
            salida = $("#residencia").val().trim()=="" ? false : true;
            changeField(validaciones, "residencia", salida);
            validaciones.push(valid);
            break;
        case 4:
            salida = $("#productType").val().trim()=="" ? false : true;
            changeField(validaciones, "productType", salida);
            salida = $("#necesidad").val().trim()=='' ? false : true;
            changeField(validaciones, "necesidad", salida);
            break;    

    }
    return !validaciones.includes(false);
}

function changeField(validaciones, campo, salida){
    validaciones.push(salida);
    if(salida){
        $('#'+campo).css("border-bottom","1px solid #009e06");
        $('#'+campo).parent().find(".alert").remove();
        
    }else{
        $('#'+campo).css("border-bottom","1px solid #f00");
        if($('#'+campo).parent().find(".alert").length){
           
        }else{
           $( '#'+campo ).after( "<p class='alert'>"+errorMesages(campo)+"</p>");
        }           
    }   
}


function errorMesages(caso){
    var mensage="";
    switch(caso) {
        case "productType":
            mensage="Escoja un tipo de producto.";
            break;          
        case "documentType":
            mensage="Escoja un documento.";
            break;
        case "documentNumber":
            mensage="Identificacion incorrecta.";
            break;        
        case "lastName":
            mensage="Campo apellido vacio.";
            break;
        case "birthDate":
            mensage="La fecha no es correcta.";
            break;
        case "cardInput":
            mensage="Por favor ingrese un archivo en formato JPG/PNG.";
            break;
        case "fecha":
            mensage="La edad no puede ser menor a 18 años.";
            break;
        case "email":
            mensage="Formato del email incorrecto.";
            break;
        case "phone":
            mensage="Movil no valido.";
            break;
        default:
            mensage="Este valor no puede estar vacio.";
    }
    return mensage;
}

function calcularEdad(year,month,day) {
    var hoy = new Date();
    var fecha=(year=="" ||month=="" ||day=="") ? "": year+"-"+month+"-"+day;
    var cumpleanos = new Date(fecha);
    var edad = hoy.getFullYear() - cumpleanos.getFullYear();
    var m = hoy.getMonth() - cumpleanos.getMonth();

    if (m < 0 || (m === 0 && hoy.getDate() < cumpleanos.getDate())) {
        edad--;
    }
    if(edad<18 || fecha==""){
        return false;
    }else{
        return true;
    }
    
}


function validate(value) {
  var validChars = 'TRWAGMYFPDXBNJZSQVHLCKET';
  var nifRexp = /^[0-9]{8}[TRWAGMYFPDXBNJZSQVHLCKET]{1}$/i;
  var nieRexp = /^[XYZ]{1}[0-9]{7}[TRWAGMYFPDXBNJZSQVHLCKET]{1}$/i;
  var str = value.toString().toUpperCase();

  if (!nifRexp.test(str) && !nieRexp.test(str)) return false;

  var nie = str
    .replace(/^[X]/, '0')
    .replace(/^[Y]/, '1')
    .replace(/^[Z]/, '2');

  var letter = str.substr(-1);
  var charIndex = parseInt(nie.substr(0, 8)) % 23;

  if (validChars.charAt(charIndex) === letter) return true;

  return false;
}

function _goToStep(wizard, options, state, index)
{
    return paginationClick(wizard, options, state, index);
}