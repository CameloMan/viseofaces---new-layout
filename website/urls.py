from django.urls import path
from . import views

urlpatterns = [
	path('', views.index, name='index'),
	path('ajax/validate_photo/', views.validate_photo, name='validate_photo'),
]