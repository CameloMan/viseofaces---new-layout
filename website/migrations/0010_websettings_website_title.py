# Generated by Django 2.2.2 on 2019-09-17 10:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0009_auto_20190917_1244'),
    ]

    operations = [
        migrations.AddField(
            model_name='websettings',
            name='website_title',
            field=models.CharField(default='Bankia :: Join Us', help_text='Enter website title.', max_length=120),
            preserve_default=False,
        ),
    ]
